﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ExitGames.Client.Photon.Chat;

public class chat_window : Photon.MonoBehaviour
{

    public string chatInput;
  
    public static chat_window SP;
    public System.Collections.Generic.List<string> messages = new System.Collections.Generic.List<string>();

   GameObject chat_input= GameObject.Find("chat_field");
    // Use this for initialization
    void Start()
    {

        

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            chatInput = this.GetComponentInChildren<Text>().ToString();
            SendChat(PhotonTargets.All);

        }

    }


    void Awake () {
        SP = this;
	}
	

    public static void AddMessage(string text)
    {
        SP.messages.Add(text);
        if (SP.messages.Count > 15)
            SP.messages.RemoveAt(0);
    }


    [PunRPC]
    void SendChatMessage(string text, PhotonMessageInfo info)
    {
        AddMessage("[" + info.sender + "] " + text);
        Debug.Log("Message Sent");
    }

    void SendChat(PhotonTargets target)
    {
       photonView.RPC("SendChatMessage", target, chatInput);
        chatInput = "";
        Debug.Log("Message Sent");
    }

    void SendChat(PhotonPlayer target)
    {

        chatInput = "[PM] " + chatInput;
        photonView.RPC("SendChatMessage", target, chatInput);
        chatInput = "";
    }
}
