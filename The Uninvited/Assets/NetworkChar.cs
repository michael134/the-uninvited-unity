﻿using UnityEngine;
using System.Collections;
 
public class NetworkChar :Photon.MonoBehaviour {

    Vector3 realpos = Vector3.zero;

    Quaternion realrot = Quaternion.identity;

	// Use this for initialization
	void Start ()
    {
       
	}
	
	// Update is called once per frame
	void Update () {

        if (photonView.isMine)
        {//nothing here
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, realpos, 0.1f);
            transform.rotation = Quaternion.Lerp(transform.rotation, realrot, 0.1f);
        }

    }

    void OnPhotonSerializeView(PhotonStream s ,PhotonMessageInfo info)
    {
        Debug.Log("OnPhotonSerializeView Called");

        if (s.isWriting)
        {
            s.SendNext(transform.position);
            s.SendNext(transform.rotation);
        }
        else
        {
            realpos = (Vector3)s.ReceiveNext();
            realrot = (Quaternion)s.ReceiveNext();

        }

    }

}
