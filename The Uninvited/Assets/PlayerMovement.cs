﻿using UnityEngine;
using System.Collections;


//only applies to local players 
public class PlayerMovement : MonoBehaviour {

    public float speed = 10f;
    float jumpspeed = 20f;
    public float currSpeed;
    float vertVelocity = 0f;
    Vector3 direction = Vector3.zero;
    CharacterController cc;
    public Animator anim;
	// Use this for initialization
	void Start () {
        cc = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        direction = transform.rotation* new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //WSAD forward back left right stored in direction
        if(direction.magnitude > 1f)
        {
            direction = direction.normalized;
        }
        currSpeed = anim.GetFloat("Speed");
        anim.SetFloat("Speed", Input.GetAxis("Vertical"));

        if(cc.isGrounded && Input.GetButtonDown("Jump"))
        {
            vertVelocity = jumpspeed;
        }

    }

    //called once per psysics loop
    void FixedUpdate()
    {

        Vector3 dist = direction * speed*Time.deltaTime;
        vertVelocity += Physics.gravity.y;
        if (cc.isGrounded)
        {
            vertVelocity = 0;
        }
        dist.y = vertVelocity * Time.deltaTime;
        cc.Move(dist);
    }
}
