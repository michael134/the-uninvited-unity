﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class level_sm : MonoBehaviour {

    //gui game objects
    GameObject pause_menu;
    GameObject pause_panel;
   
  
    GameObject cross;
    GameObject chat;
    GameObject debug;
    GameObject options;

    // Use this for initialization
    void Start () {
       
        //find our game objects 
        pause_menu = GameObject.Find("pause_menu");
        cross = GameObject.Find("crosshair");
        debug = GameObject.Find("Debug");
        options = GameObject.Find("options_menu");
        chat = GameObject.Find("chat_panel");

        //set the game objects to active or inactive accordingly
        pause_menu.SetActive(false);
       
        chat.SetActive(false);
        Cursor.visible = false;
        debug.SetActive(false);
        options.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {


        //show our pause menu
        if (Input.GetKeyDown(KeyCode.Escape) && pause_menu.GetActive()==false && chat.GetActive()==false)
        {
            pause_menu.SetActive(true);
            cross.SetActive(false);
            Cursor.visible = true;
            options.SetActive(true);
            

        }
        else if (Input.GetKeyDown(KeyCode.Escape) && pause_menu.GetActive() == false && chat.GetActive() == true) 
         { chat.SetActive(false);
            
        }

        else if (Input.GetKeyDown(KeyCode.Escape) && pause_menu.GetActive() == true)
        {
            cross.SetActive(true);
            Cursor.visible = false;
            options.SetActive(false);
            pause_menu.SetActive(false);

        }

        //debugging info for F3 key
        if (Input.GetKeyDown(KeyCode.F3)&& debug.GetActive()==false)
        {
            debug.SetActive(true);

        }
        else if(Input.GetKeyDown(KeyCode.F3) && debug.GetActive() == true)
        { debug.SetActive(false); }


        if (Input.GetKeyDown(KeyCode.T) && chat.GetActive() == false)
        {

            chat.SetActive(true);
            //Cursor.lockState = CursorLockMode.Locked;
            InputField chat_field = chat.GetComponentInChildren<InputField>();
            chat_field.Select();
            chat_field.ActivateInputField();

        }


    }

    
    //quit our application 
    public void gtfo() { Application.Quit(); }


    //load up another game scene
    public void change_scene(string next_scene)
    {
        Application.LoadLevel(next_scene);
        
    }


    //display our networking information as it comes in 
    //tmp
    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
    }



}
