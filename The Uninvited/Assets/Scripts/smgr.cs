﻿using UnityEngine;
using System.Collections;

public class smgr : MonoBehaviour {

    private GameObject panel;

    public void Start()
    {
        panel = GameObject.Find("Panel");
        panel.SetActive(false);
    }

    public void change_scene(string next_scene)
    {
        Application.LoadLevel(next_scene);
    }

    public void gtfo()
    {
        Application.Quit();
    }


    public void displayCredits(int isShown)
    {
        if (isShown== 0)
        {

            panel.SetActive(false);
        }
        else
            panel.SetActive(true);
    }
}
