﻿using UnityEngine;
using System.Collections;

public class network_manager : MonoBehaviour {

    public GameObject log_cam;
    public string room = "v0.1.1Dev_";
    SpawnPoint[] spawns;
 
   
    void Start () {
        connect();
        spawns = FindObjectsOfType<SpawnPoint>();
        
    }
	
	void connect()
    {
       
        PhotonNetwork.ConnectUsingSettings("v0.1.1");
        
    }
    void OnConnectedToMaster()
    {
        
        PhotonNetwork.JoinRoom(room);
        Debug.Log("Player Joined Master");

    }
    void OnJoinedLobby()
    {
        
        
      
    }
   
    void OnJoinedRoom()
    {
        Debug.Log("Player Joined Room");
        Spawn();

    }
    void OnPhotonCreateGameFailed()
    {
        PhotonNetwork.JoinRoom(room);
    }

    void Spawn()
    {
        Debug.Log("Player Spawn");
        //spawn player in one of these locations
       SpawnPoint MySpawn = spawns[Random.Range(0,spawns.Length-1)];
     
        GameObject myPlayerGO = PhotonNetwork.Instantiate("playercontroller4", MySpawn.transform.position,Quaternion.identity,0);
        log_cam.SetActive(false);//disable camera on successful login
        myPlayerGO.GetComponent<CharacterController>().enabled = true;
        myPlayerGO.GetComponentInChildren<Camera>().enabled = true;
     
        ((MonoBehaviour)myPlayerGO.GetComponent("FirstPersonController")).enabled = true;
        ((MonoBehaviour)myPlayerGO.GetComponent("PlayerMovement")).enabled = true;
    }

    void OnPhotonJoinRoomFailed()
    {

        PhotonNetwork.CreateRoom(room);
    }
}
